import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import { useTabStore } from '@/stores/tab'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: {
        title: '首页'
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue'),
      meta: {
        title: '登录',
        layout: 'empty'
      }
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('../views/RegisterView.vue'),
      meta: {
        title: '注册',
        layout: 'empty'
      }
    },
    {
      path: '/learn-square',
      name: 'learn-square',
      component: () => import('../views/LearnSquareView.vue'),
      meta: {
        title: '学习广场'
      }
    },
    {
      path: '/course-learn/:id',
      name: 'course-learn',
      component: () => import('../views/CourseLearnView.vue'),
      meta: {
        title: '课程学习'
      }
    },
    {
      path: '/course-grade/:id',
      name: 'course-grade',
      component: () => import('../views/CourseGradeView.vue'),
      meta: {
        title: '课程成绩'
      }
    }
  ]
})

router.beforeResolve((to, from, next) => {
  const { addTab, setActiveTab } = useTabStore()
  if (to.meta.layout === 'empty') {
    next()
    return
  }
  if (to.matched.length === 0) {
    next(from)
    return
  }
  addTab({
    name: String(to.meta.title),
    path: to.path,
    isCloseable: true
  })
  setActiveTab(to.path)
  next()
})

export default router
