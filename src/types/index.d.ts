declare module 'common-list' {
  import type { AxiosInstance, AxiosRequestConfig } from 'axios'
  import { App, Component, type Ref } from 'vue'

  export declare function InitComponentData<T>(
    nettoolThis: NetTool,
    urlSet: CrudUrlSet,
    keyFieldName: string,
    params?: SearchParam,
    rowEditCheckFunc?: (row: T) => {},
    autoGetList?: boolean,
    withHint?: boolean
  ): {
    tableData: Ref<T>
    totalCount: import('vue').Ref<number>
    loading: import('vue').Ref<boolean>
    selectedRows: import('vue').Ref<never[]>
    onSave: (objdata: T | FormData, saveType: SaveType, config: AxiosRequestConfig) => Promise<any>
    refreshList: (newParam: any) => Promise<void | import('../models/index').ResultList<T>>
    setRequestParams: (newParam: any) => void
  }

  export declare class NetTool {
    axios: AxiosInstance

    constructor(axiosObj: AxiosStatic)

    isResultList<T>(data: any): data is ResultList<T>

    getList<
      T extends {
        [key: string]: string | number
      }
    >(urlSet: CrudUrlSet, params: SearchParam, useCache?: boolean): Promise<ResultList<T> | void>

    getListData<
      T extends {
        [key: string]: string | number
      }
    >(urlSet: CrudUrlSet, params: SearchParam, useCache?: boolean): Promise<T[]>

    /**
     * 新增数据
     * @url api请求地址
     * @param urlSet
     * @param objData 要新增的对象，必传
     * @param params
     * @param config
     * @returns 返回接口数据
     */
    addEntity<T>(
      urlSet: CrudUrlSet,
      objData: T,
      params: SearchParam,
      config: AxiosRequestConfig
    ): Promise<ResultType<T> | void>

    multiAddEntity<T>(urlSet: CrudUrlSet, objData: T, params: SearchParam): Promise<T | undefined>

    /**
     * 检查是否存在数据
     * @url api请求地址
     * @param urlSet
     * @param objData 要检查的对象，必传
     * @returns 返回接口数据
     */
    checkEntity<T>(urlSet: CrudUrlSet, objData: T): Promise<ResultType<T>>

    /**
     * 修改数据
     * @url api请求地址
     * @param urlSet
     * @param objData 要修改的对象，必传
     * @param params
     * @param config
     * @returns 返回接口数据
     */
    putEntity<T>(
      urlSet: CrudUrlSet,
      objData: T,
      params: SearchParam,
      config: AxiosRequestConfig
    ): Promise<T>

    returnResultObj(result: any): any

    /**
     * 批量修改数据
     * @url api请求地址
     * @param urlSet
     * @param objData 要修改的对象，必传
     * @param params
     * @returns 返回接口数据
     */
    multiSetEntity<T>(
      urlSet: CrudUrlSet,
      objData: BatchSetModel<T>,
      params: SearchParam
    ): Promise<T | null>

    /**
     * 删除数据
     * @url api请求地址
     * @returns 返回接口数据
     * @param urlSet
     * @param delId
     * @param params
     */
    delEntity<T>(urlSet: CrudUrlSet, delId: number | string, params: SearchParam): Promise<T>

    /**
     * 获取单条记录
     * @url api请求地址
     * @returns 返回接口数据
     * @param urlSet
     * @param entityId
     * @param useQuery
     * @param subjectID
     * @param useCache
     */
    getEntity<T>(
      urlSet: CrudUrlSet,
      entityId: number | string,
      useQuery?: boolean,
      subjectID?: number,
      useCache?: boolean
    ): Promise<T | null | undefined>

    /**
     * 上传数据
     * @param urlSet
     * @param objData
     * @param params 要传的参数值，非必传
     * @param config
     * @returns 返回接口数据
     */
    uploadEntities(
      urlSet: CrudUrlSet,
      objData: FormData,
      params: SearchParam,
      config: AxiosRequestConfig
    ): Promise<any>
  }

  /**
   * window.localStorage 浏览器永久缓存
   * @method set 设置永久缓存
   * @method get 获取永久缓存
   * @method remove 移除永久缓存
   * @method clear 移除全部永久缓存
   */
  export declare const Local: {
    set(key: string, val: any, expired?: number): void
    get(key: string, needParse?: boolean): any
    remove(key: string): void
    clear(): void
  }

  /**
   * window.sessionStorage 浏览器临时缓存
   * @method set 设置临时缓存
   * @method get 获取临时缓存
   * @method remove 移除临时缓存
   * @method clear 移除全部临时缓存
   */
  export declare const Session: {
    set(key: string, val: any, expired?: number): void
    get(key: string, needParse?: boolean): any
    remove(key: string): void
    clear(): void
  }

  export declare const ClientStorage: {
    set(
      key: string,
      val: any,
      isLocalStorage?: boolean,
      needEncode?: boolean,
      expired?: number
    ): void
    remove(key: string, isLocalStorage?: boolean): void
    get(key: string, isLocalStorage?: boolean, needDecode?: boolean): any
  }

  export declare function filterArrayByText(
    searchText: string,
    dataArr: [],
    columnList: DataColumn[]
  ): never[]

  export declare function isBeforeTheTime(dateTimeStr: any, addSeconds?: number): boolean

  export declare function isEmptyObj(obj: any): boolean

  export declare const formatDate: (dateval: string | number, needTime?: boolean) => any

  export declare const exportXLS: (tableID: string, fileNamePrefix: string) => Promise<any>

  export declare enum SaveType {
    Add,
    Edit,
    Delete,
    MultiSet,
    Upload,
    Check
  }

  export declare interface KeyValue {
    title: string
    value: string | number
  }

  export declare interface DataColumn {
    //数据列类型
    title: string
    fieldName: string
    placeholder?: string
    inputType?: string //input的数据类型，对应input ： text,number,date
    options?: KeyValue[]
    isMultiple?: boolean
    required?: boolean
    useSwitch?: boolean
    dataType?: string
    isMainField?: boolean //是否需要占用多点显示位置，如35%
    isHideInList?: boolean //是否不在表格里显示
    isHideInForm?: boolean //是否不在表单里显示
    isImg?: boolean
    isDate?: boolean
    needShowTime?: boolean
    isWholeRow?: boolean //是否独占一行！！！
    isRichText?: boolean
    isFile?: boolean
    isAutoUpload?: boolean
    fileLimit?: number
    useComponent?: boolean
    canEditInTable?: boolean
    showInDetail?: boolean
  }

  export declare class CrudUrlSet {
    getListApiUrl?: string
    getSingleApiUrl?: string
    addApiUrl?: string
    editApiUrl?: string
    delApiUrl?: string
    checkApiUrl?: string // 用于登录
    multiSetApiUrl?: string // 批量重置
    multiAddApiUrl?: string
    resetListApiUrl?: string // 批量重置修复
    importApiUrl?: string // 批量导入
    restfulApiUrl?: string // 统一api地址，根据http method去辨别！
    keyFieldName: string
    subjectFieldName?: string
    title: string
    needResetKeysWhenUpdated?: string[]

    constructor(
      getListApiUrl: string,
      getSingleApiUrl: string,
      addApiUrl: string,
      editApiUrl: string,
      delApiUrl: string,
      checkApiUrl: string,
      importApiUrl: string,
      keyFieldName: string,
      title: string
    )
  }

  export declare interface FilterData {
    datalist: any[] //这个是数据数组
    selectedlist?: any[] | any //这个选中数据数组
    isMultiple: boolean //是否可以多选
    needSelected?: boolean //必须选择一项！
    onlyLocal?: boolean //是否只是本地筛选
    placeholder: string //占位符文本
    filterFieldName: string //列表筛选字段名，这个是必选的
    valueFieldName?: string //选项值的字段名
    textFieldName?: string //选项文本的字段名
  }

  export declare interface TableListOption<T> {
    tableID?: string //表格ID,用于导出
    maxHeight?: number //最大高度
    opColumnWidth?: number //操作栏宽度,默认200
    opColumnTitle?: string //操作栏标题
    highlightCurrentRow?: boolean //高亮显示当前行
    noOpColumn?: boolean //不显示操作栏
    useSelect?: boolean //使用选择，默认为true
    singleSelect?: boolean //单选，在useSelect为true是时才有意义
    resizable?: boolean //允许调宽
    canEdit?: boolean //表格中直接显示编辑，一般是配合editChange方法
    paging?: boolean //是否分页，数据总数量与tableData的数量不一致时为true,
    searchText?: string //来自ListHead的搜索文字
    showDelOpBtns?: boolean //显示删除按钮，默认为true
    showDetailOpBtns?: boolean //显示详情按钮，默认为true
    showEditOpBtns?: boolean //显示编辑按钮，默认为true
    columnList: DataColumn[] //字段属性数组，表格数据根据这个来显示
    tableData: T[] //表格数据源
    totalCount?: number //数据总数量，如果与tableData的数量不一致时，则显示分页
    downloadFile?: (url: string) => {} //参数传过来是方法！！
    pageSizes?: number[] //页码设置，默认为[20, 30, 50, 100]
  }

  export declare interface ListHeadOption<T> {
    searchplaceholder?: string //搜索框占位符文字
    sampleFileUrl?: string //导入的样例文件
    addBtnText?: string //新增按钮的文本，默认为添加
    tableID?: string //表格ID,用于导出
    enableBatchSet?: boolean //支持批量设置
    hasResizeBtn?: boolean //  显示调整表格宽度的按钮
    enableMultiAdd?: boolean //支持批量添加
    isCustomRefresh?: boolean //自定义重新刷新列表，自定义读取数据api
    hasSearchInput?: boolean //显示搜索框
    showImportBtn?: boolean //显示导入按钮
    enableExport?: boolean //显示导出
    enableColumnBtn?: boolean //管理显示列
    searchFromServer?: boolean //从服务器上搜索
    filterFromServer?: boolean //通过在服务器里筛选！！
    showDefaultOpBtns?: boolean //显示默认按钮，默认为true
    columnList: DataColumn[] //字段属性数组，表格数据根据这个来显示
    tableData: T[] //表格数据源
    totalCount?: number //数据总数量，如果与tableData的数量不一致时，则显示分页
    filterObjList?: FilterData[] //筛选数据列表，可以有多个，可选单选或多选
  }

  export declare interface CommonListOption<T> extends TableListOption<T>, ListHeadOption<T> {
    dialogWidth?: number //对话框宽度，默认1000
    datatitle?: string //对话框的标题
    keyFieldName?: string //主键字段名
    maxImgSize?: number // 图片大小最大字节， 默认2 * 1024 * 1024
    maxVideoSize?: number // 视频大小最大字节， 默认50 * 1024 * 1024
    hasListHead?: boolean //是否包含列表头
    newDialogData?: boolean //使用新的对话框数据，默认为true
    showDialogSizeSet?: boolean //显示对话框大小设置按钮
    showEditInTable?: boolean //表格中直接显示编辑，一般是配合editChange方法
    containDialog?: boolean //是否包含对话框，如果只显示数据，可以设置这个为true,已节省组件开销
    showKeepData?: boolean //保存之前的表单数据
    isDemo?: boolean //是否用于demo显示
    addNewObj?: T //新建对象
    autoOpenObj?: T //自动打开的对象，一般为接受其他系统传过来的参数
    listStyle: string //列表显示方式，分别为table(表格),pic(图片列表),free(自由列表)
    getRowData?: (n: number) => {} //参数传过来是方法！用于获取单条记录，一般从服务器重新获取！
    settingColumnList?: DataColumn[] //设置后，ListHead的enableBatchSet将为true
  }

  // 定义每个组件的接口
  interface TableList extends Component {}

  interface PicShowList extends Component {}

  interface FreeList extends Component {}

  interface ListHead extends Component {}

  interface SaveDialog extends Component {}

  interface DetailDialog extends Component {}

  interface WEditor extends Component {}

  interface FileUpload extends Component {}

  interface ImgUpload extends Component {}

  interface CommonList extends Component {}

  // 定义 install 函数的类型
  interface Install {
    (app: App): void
  }

  // 定义默认导出的类型
  interface DefaultExport {
    0: TableList
    1: PicShowList
    2: FreeList
    3: ListHead
    4: SaveDialog
    5: DetailDialog
    6: WEditor
    7: FileUpload
    8: ImgUpload
    9: CommonList
    install: Install
  }

  const commonList: DefaultExport
  export default commonList
}
