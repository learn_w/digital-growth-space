export interface Result<T> {
  errno: number
  data: T
}

export interface Course {
  currNo: number
  name: string
  currDesc: string
  iconPath: string
  startTeachingTime: string
  finishTeachingTime: string
  choosedCount: number
  mainDesignerUserName: string
  courseData: CourseData
}

export interface CourseData {
  goal: string
  iconPath: string
  taskGroups: Record<string, TaskGroup>
}

export interface TaskGroup {
  name: string
  iconPath: string
  groupNoInCourse: number
  tasks: Record<string, Task>
}

export interface Task {
  taskID: number
  taskDeadline: string
  goal: string
  name: string
  description: string
  isFinalTask: boolean
  iconPath: string
  taskNoInGroup: number
  guidelines: Record<string, Guidelines>
}

export interface Guidelines {
  noInTask: number
  operType: string
  description: string
}

export interface CourseGrades {
  userID: number
  realName: string
  className: string
  stuNum: string
  taskName: string
  submitContent: string
  taskScore: string | number
  taskComment: string
  calscore: string
}
