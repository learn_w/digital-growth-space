import './assets/style.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn'

import Vant from 'vant'
import 'vant/lib/index.css'

import CommonList from 'common-list'

const app = createApp(App)

app.use(ElementPlus, {
  locale: zhCn
})
app.use(Vant)

app.use(createPinia())
app.use(router)

app.use(CommonList)

app.mount('#app')
