import { useTabStore } from '@/stores/tab'
import router from '@/router'

export function useTabName(name?: string) {
  const tabStore = useTabStore()

  const changeTabName = () => {
    router.currentRoute.value.meta.title = name

    tabStore.tabs.map((tab) => {
      if (tab.path === router.currentRoute.value.path) {
        return (tab.name = name ?? '课程学习')
      }
      return tab
    })
  }
  changeTabName()
}
