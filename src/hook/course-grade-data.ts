import { computed, onMounted, ref, watch } from 'vue'
import type { CourseGrades, Result, Task } from '@/request/types'
import axios from '@/request/request'
import {
  type CrudUrlSet,
  type DataColumn,
  type FilterData,
  InitComponentData,
  NetTool
} from 'common-list'
import router from '@/router'

export function useCourseGradeData() {
  const courseId = ref(Number(router.currentRoute.value.params.id))
  const allTask = ref<Task[]>()
  const selectedTaskName = ref<string>('')
  const requestAllTask = async () => {
    const result = await axios.get<Result<Task[]>>(
      `/api/queryresult/getalltasks?currNo=${courseId.value}`
    )
    allTask.value = result.data.data
    selectedTaskName.value = allTask.value[0].name

    await refreshList({
      currNo: courseId.value,
      taskID: allTask.value[0].taskID
    })
  }

  onMounted(async () => {
    await requestAllTask()
  })

  // 监听路由改变用于切换标签页时刷新数据
  watch(router.currentRoute, async () => {
    if (router.currentRoute.value.name === 'course-grade') {
      courseId.value = Number(router.currentRoute.value.params.id)
      await requestAllTask()
    }
  })

  // 请求工具
  const netTool = new NetTool(axios)
  // 请求url
  const curdUrl: CrudUrlSet = {
    getListApiUrl: '/api/queryresult/studylist',
    restfulApiUrl: '',
    importApiUrl: '',
    keyFieldName: '',
    title: ''
  }
  // 获取初始数据
  const { tableData, totalCount, refreshList } = InitComponentData<
    Result<CourseGrades[] | undefined>
  >(netTool, curdUrl, 'userID', { currNo: courseId.value, taskID: 0 })

  // 设置param，刷新数据
  watch(selectedTaskName, async () => {
    if (!allTask.value) return
    const selectedTask =
      allTask.value.find((item) => item.name === selectedTaskName.value) ?? allTask.value[0]
    await refreshList({
      currNo: courseId.value,
      taskID: selectedTask.taskID
    })
  })

  const columnData = ref<DataColumn[]>([
    {
      title: '姓名',
      fieldName: 'realName'
    },
    {
      title: '班级',
      fieldName: 'className',
      options: []
    },
    {
      title: '学号',
      fieldName: 'stuNum'
    },
    {
      title: '成绩',
      fieldName: 'taskScore'
    }
  ])
  const cColumnData = computed(() => {
    return columnData.value.map((item) => {
      return item
    })
  })

  const filterDataList = ref<FilterData[]>([
    {
      placeholder: '课程',
      datalist: [],
      isMultiple: false,
      onlyLocal: false,
      filterFieldName: 'taskName',
      needSelected: true,
      selectedlist: selectedTaskName
    },
    {
      placeholder: '班级',
      datalist: [],
      isMultiple: false,
      onlyLocal: true,
      filterFieldName: 'className'
    }
  ])
  const cFilterDataList = computed(() => {
    return filterDataList.value.map((item) => {
      switch (item.filterFieldName) {
        case 'taskName':
          item.datalist = allTask.value?.map((item) => item.name) ?? []
          break
        case 'className':
          item.datalist = [...new Set(tableData.value.data?.map((item) => item.className) ?? [])]
          break
      }
      return item
    })
  })

  const rowClassFunc = ({ row }: { row: CourseGrades }) => {
    switch (row.taskScore) {
      case 95:
        return 'row-95'
      case 85:
        return 'row-85'
      case 75:
        return 'row-75'
      case 65:
        return 'row-65'
      case 55:
        return 'row-55'
      case 45:
        return 'row-45'
      default:
        return ''
    }
  }

  return { tableData, totalCount, cColumnData, cFilterDataList, rowClassFunc }
}
