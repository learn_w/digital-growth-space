/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './index.html',
    './src/**/*.{vue,js,ts,jsx,tsx}'
  ],
  theme: {
    container: {
      padding: '1rem'
    },
    extend: {
      screens: {
        '2xl': '1440px',
      },
    }
  },
  plugins: []
}

